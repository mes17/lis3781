> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Managements

## Matthew Stoklosa

### Project 2 Requirements:

*Four parts:*

1. Questions
2. Download MongoDB
3. MongoDB Create & Insert Database
    - Add MongoDB Array using insert()
    - Mongodb ObjectId()
    - MongoDB Query Document using find()
    - MongoDB cursor
    - MongoDB Query Modifications using limit(), sort()
    - MongoDB Count() & remove() function
    - MongoDB Update() Document
4. Bitbucket repo link to this assignment

#### README.md file should include the following items:

- screenshot of P2 shell command
- Screenshot of P2 Show Report
- P2 JSON file

#### Assignment Screenshots and Documents:

*Screenshot of P2 shell command*:

![Screenshot of P2 shell command](img/p2_sc.png)

*Screenshot of P2 Show Report*:

![Screenshot of P2 Show Report](img/p2_sr.png)

*P2 docs: P2.json*:

[P2 JSON](docs/a5.json "A5 SQL code")
