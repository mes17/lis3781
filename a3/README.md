> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Managements

## Matthew Stoklosa

### Assignment 3 Requirements:

*Four parts:*

1. log into Oracle Server
2. Screenshotof *your* SQL code
3. Screenshotof *your* populated tables (w/in the Oracle environment)
4. SQL code for the required reports.

#### README.md file should include the following items:

• Screenshot of A3 code

• Screenshot of A3 populated tables

#### Assignment Screenshots:

*Screenshot of A3 sql code*:

![Screenshot of A3 sql code](img/a3_code_1.png)

*Screenshot of A3 sql code*:

![Screenshot of A3 sql code](img/a3_code_2.png)

*Screenshot of A3 sql code*:

![Screenshot of A3 sql code](img/a3_code_3.png)

*Screenshot of A3 populated tables*:

![Screenshot of A3 populated tables](img/a3_tables.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A3 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")