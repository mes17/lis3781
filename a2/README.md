> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Managements

## Matthew Stoklosa

### Assignment 2 Requirements:

*Two parts:*

1. SQL code
2. Questions


#### A2 Deliverables Parts:

A. Tables and insert statements.

B. Include indexes and foreign key SQL statements(see below).

C. Include your query resultsets, including grant statements.

D. The following tables should be created and populated with at least 5 records bothlocally and to the CCI server. 

#### README.md file should include the following items:

• Screenshot of A2 code

• Screenshot of A2 populated tables

#### Assignment Screenshots:

*Screenshot of A2 code*:

![Screenshot of A2 code](img/a2_code.png)

*Screenshot of A2 code part 2*:

![Screenshot of A2 code part 2](img/a2_code_2.png)

*Screenshot of A2 populated tables*:

![Screenshot of A2 populated tables](img/a2_pt.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")