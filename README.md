> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Managements

## Matthew Stoklosa

### LIS3781  Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/mes17/lis3781/src/master/a1/README.md)

    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - provide git command descriptions

2. [A2 README.md](https://bitbucket.org/mes17/lis3781/src/master/a2/README.md)

    - Tables and insert statements.
    - Include indexes and foreign key SQL statements(see below).
    - Include your query resultsets, including grant statements.
    - The following tables should be created and populated with at least 5 records bothlocally and to the CCI server.

3. [A3 README.md](https://bitbucket.org/mes17/lis3781/src/master/a3/README.md)

    - log into Oracle Server
    - Screenshotof *your* SQL code
    - Screenshotof *your* populated tables (w/in the Oracle environment)
    - SQL code for the required reports.

4. [A4 README.md](https://bitbucket.org/mes17/lis3781/src/master/a4/README.md)

    - Business Rules
    - Questions
    - Entity Relationship Diagram
    - SQL Code

5. [A5 README.md](https://bitbucket.org/mes17/lis3781/src/master/a5/README.md)

    - Business Rules
    - Questions
    - Entity Relationship Diagram
    - SQL Code

6. [P1 README.md](https://bitbucket.org/mes17/lis3781/src/master/p1/README.md)

    - Populated tables
    - Entity Relationship Diagram, and SQL Statements
    - Business Rules
    - Questions

7. [P2 README.md](https://bitbucket.org/mes17/lis3781/src/master/p2/README.md)

    - MongoDB Create & Insert Database
    - Add MongoDB Array using insert()
    - Mongodb ObjectId()
    - MongoDB Query Document using find()
    - MongoDB cursor
    - MongoDB Query Modifications using limit(), sort()
    - MongoDB Count() & remove() function
    - MongoDB Update() Document
    - Questions

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")