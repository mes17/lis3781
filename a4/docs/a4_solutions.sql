--1) Create a view that displays the sum of all paid invoice totals for each customer, sort by the largest invoice total sum appearing first.

use mjowett;

go

--a snapshot of all invoices
select * from dbo.invoice;

--b 
select inv_id, inv_total as paid_invoice_total
from dbo.invoice
where inv_paid !=0;

--c snapshot of sum of all *paid* invoices;
select sum(inv_total) as sum_paid_invoice_total
from dbo.invoice
where inv_paid !=0;

print'#1 Solution: create view (sum of each customer''s *paid* invoices, in desc order):

';

--drop view if exists
--1st arg is object name, 2nd arg is type (V=view)
IF OBJECT_ID (N'dbo.v_paid_invoice_total', N'V') IS NOT NULL
DROP VIEW dbo.v_paid_invoice_total;
GO

--In MS SQL SERVER: do *NOT* use ORDER BY clause in * VIEWS* (non-guaranteed behavior)
create view dbo.v_paid_invoice_total as 
    select p.per_id, per_fname, per_lname, sum(inv_total) as sum_total, FORMAT(sum(inv_total),'C', 'em-us') as v_paid_invoice_total
    from dbo.person p
        join dbo.customer c on p.per_id=c.per_id=c.per_id
        join dbo.contact ct on c.per_id=ct.per_cid
        join dbo.[order] o on o.ord_id=i.ord_id
        join dbo.invoice i on o.ord_id=i.ord_id
    where inv_paid !=0

--must be contained in group by, if not used in aggreagate function
    group by p.per_id, per_fname, per_lname
go

-- display view results (order by should be used outside of view)
select per_id, per_fname, per_lname, v_paid_invoice_total
from dbo.v_paid_invoice_total
order by sum_total desc;
go

--double-check! Check individaul customer paid invoices!
select p.per_id, per_fname, per_lname, inv_total, inv_paid
from dbo.person p
    join dbo.customer c on p.per_id=c.per_id=c.per_id
    join dbo.contact ct on c.per_id=ct.per_cid
    join dbo.[order] o on o.ord_id=i.ord_id
    join dbo.invoice i on o.ord_id=i.ord_id;

--compare views to base tables
SELECT * from information_schema.tables;
go

--display definition of trigger, stored procedure, or view
sp_helptext 'dbo.v_paid_invoice_total'
go

--remote view from server memory
drop view dbo.v_paid_invoice_total;

--2) Create a stored procedure that displays all customers’ outstanding balances (unstored derived attribute based upon the difference of a 
--customer's invoice total and their respective payments). List their invoice totals, what was paid, and the difference.


--a. individual customer
select p.per_id, per_fname, per_lname,
sum(pay_amt) as total_paid, (inv_total - sum(pay_amt)) invoice_diff
    from person p
    join dbo.customer c on p.per_id=c.per_id=c.per_id
    join dbo.contact ct on c.per_id=ct.per_cid
    join dbo.[order] o on o.ord_id=i.ord_id
    join dbo.invoice i on o.ord_id=i.ord_id
    join dbo.payment pt on i.inv_id=pt.inv_id
where p.per_id=7-- might need a ";"

--must be contained in group by, if not used in agregate function
group by p.per_id, per_fname, per_lname, inv_total;

print'#1 Solution: create procedure (display all customers'' outstanding balances):

';

--1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.sp_all_customers_obstanding_balances',N'P') IS NOT NULL
DROP PROC dbo.sp_all_customers_obstanding_balances
GO

----In MS SQL SERVER: do *NOT* use ORDER BY clause in stored procedures though, *not* views
CREATE PROC dbo.sp_all_customers_obstanding_balances AS
begin
  select p.per_id, per_fname, per_lname
  sum(pay_amt) as total_paid, (inv_total - sum(pay_amt)) invoice_diff
  from person p
    join dbo.customer c on p.per_id=c.per_id=c.per_id
    join dbo.contact ct on c.per_id=ct.per_cid
    join dbo.[order] o on o.ord_id=i.ord_id
    join dbo.invoice i on o.ord_id=i.ord_id
    join dbo.payment pt on i.inv_id=pt.inv_id
--must be contained in group by, if not used in aggreagate function
group by p.per_id, per_fname, per_lname, inv_total
order by invoice_diff desc;
END
GO

-- call stored procedure (Note: negative values mean *customer* is owed money or credit!)
exec dbo.sp_all_customers_obstanding_balances;

--list all procedure (e.g., stored procedures or functions) for database
select * from mjowett.information_schema.routines
where routine_type = 'PROCEDURE';
go


--3) Create a stored procedure that populates the sales rep history table w/sales reps’ data when called.

print'#3 Solution: create stored procedure to popluate history table w/sales reps'' data when called:

';

--1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.sp_populate_srp_hist_table',N'P') IS NOT NULL
DROP PROC dbo.sp_populate_srp_hist_table
GO

CREATE PROC dbo.sp_populate_srp_hist_table AS
BEGIN
    INSERT INTO dbo.srp_hist
    (per_id, sht_type, sht_modified, sht_modifer, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)

--mix dynamically generated data, with orginal sales reps' data
    Select per_id, 'i', getDate(), SYSTME_USER, getDate(), srp_yr_sales_goal, srp_yrt_sales, srp_ytd_sales, srp_ytd_comm, srp_notes
    FROM dbo.slsrep;
END
GO

print 'list table data before call:

';
select * from dbo.slsrep;

-- Purposefully delting orignal data to simulate iniially populating a "log" or history table
delete from dbo.srp_hist;

select * from dbo.srp_hist;  --show that there is no data!

--call stored procduer (popilated srp_hist_table with slsrep table data)


--4) Create a trigger that automatically adds a record to the sales reps’ history table for every record
--added to the sales rep table.

--1st arg is object name, 2nd arg is type (TR=trigger)
IF OBJECT_ID(N'dbo.trg_sales_history_insert', N'TR') IS NOT NULL
  DROP TRIGGER dbo.trg_sales_history_insert
  go

  create trigger dbo.trg_sales_history_insert
  ON dbo.slsrep
  AFTER INSERT AS
  BEGIN
  --declare
  DECLARE
  @per_id_v smallint,
  @sht_type_v char(1),
  @sht_modified_v date,
  @sht_modifier_v varchar(45),
  @sht_date_v date,
  @sht_yr_sales_goal_v decimal(8,2),
  @sht_yr_total_sales_v decimal(8,2),
  @sht_yr_total_comm_v decimal(7,2),
  @sht_notes_v varchar(255);

  SELECT
  @per_id_v = per_id,
  @sht_type_v = 'i',
  @sht_modified_v = getDate(),
  @sht_modifier_v = SYSTEM_USER,
  @sht_date_v  = getDate(),
  @sht_yr_sales_goal_v = srp_yr_sales_goal,
  @sht_yr_total_sales_v = srp_ytd_sales,
  @sht_yr_total_comm_v = srp_ytd_comm,
  @sht_notes_v = srp_notes
  from inserted;

  INSERT INTO dbo.srp_hist
  (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_year_total_comm, sht_notes)
  VALUES
  (@per_id_v, @sht_type_v, @sht_modified_v, @sht_modifier_v, @sht_date_v, @sht_yr_sales_goal_v, @sht_yr_total_sales_v, @sht_yr_total_comm_v, @sht_notes_v);
  END
  GO

  print 'list table data before trigger fires:
  ';
  select * from dbo.slsrep;
 select * from dbo.srp_hist;

 INSERT INTO dbo.slsrep
 (per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
 VALUES
 (6, 98000, 43000, 8750, 'per_id values 1-5 already used');

 print 'list table data after trigger fires:
 ';
 select * from dbo.slsrep;
 select * from dbo.srp_hist;

 select * from sys.triggers;
 go


--5) Create a trigger that automatically adds a record to the product history table for every record
--added to the product table.

--1st arg is object name, 2nd arg is type (TR=trigger)
IF OBJECT_ID(N'dbo.trg_product_history_insert', N'TR') IS NOT NULL
  DROP TRIGGER dbo.trg_product_history_insert
  go

  create trigger dbo.trg_product_history_insert
  ON dbo.product
  AFTER INSERT AS
  BEGIN
  DECLARE
  @pro_id_v smallint,
  @pht_modified_v date,
  @pht_cost_v decimal(7,2),
  @pht_price_v decimal(7,2),
  @pht_discount_v decimal(3,0),
  @pht_notes_v varchar(255);

  SELECT
   @pro_id_v = pro_id,
  @pht_modified_v = getDate(),
  @pht_cost_v = pro_cost,
  @pht_price_v = pro_price,
  @pht_discount_v = pro_discount,
  @pht_notes_v = pro_notes
  from inserted;

  INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)  
  VALUES
  (@pro_id_v, @pht_modified_v, @pht_cost_v, @pht_price_v, @pht_discount_v, @pht_notes_v);
  END
  GO



  print 'list table data before trigger fires:
  ';
  select * from product;
 select * from product_hist;

 INSERT INTO dbo.product
 (ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
 VALUES
 (3, 'desk lamp', 'small desk lamp with led lights', 3.6, 14, 5.98, 11.99, 15, 'No discounts after sale.');

 print 'list table data after trigger fires:
 ';
 select * from product;
 select * from product_hist;

 select * from sys.triggers;
 go

--***Extra Credit***
--Create a stored procedure that updates sales reps’ yearly_sales_goal in the slsrep table, based upon 8% more than their previous year’s total sales 
--(sht_yr_total_sales), name it sp_annual_salesrep_sales_goal. (See Notes above.)

--1st arg is object name, 2nd arg is type (P=procedure)
IF OBJECT_ID(N'dbo.sp_annual_salesrep_sales_goal',N'P') IS NOT NULL
DROP PROC dbo.sp_annual_salesrep_sales_goal
GO

CREATE PROC dbo.sp_annual_salesrep_sales_goal AS
BEGIN
 UPDATE slsrep
 SET srp_yr_sales_goal = sht_yr_total_sales * 1.08
 from slsrep as sr
  join srp_hist as sh
  on sr.per_id = sh.per_id
where sht_date=(select max(sht_date) from srp_hist);
END
GO

print 'list table data before call:
 ';
 select * from dbo.slsrep;
 select * from dbo.srp_hist;

 exec dbo.sp_annual_salesrep_sales_goal;

 print 'list table data after call:
 ';
select * from dbo.slsrep;

 select * from dsh16c.information_schema.routines
  where routine_type = 'PROCEDURE';
  go