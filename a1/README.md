> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Managements

## Matthew Stoklosa

### Assignment 1 Requirements:

*Five parts:*

1. Distibuted Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL Code (optional)
5. Bitbucket repo links:
    a) this assignment and 
    b) the complete tutorial (bitbucketstationlocations)


#### A1 Database Business Rules:

The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes:job description, length of employment, benefits,number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories mustbe tracked. Also, include the following business rules:

• Each employee may have one or more dependents.

• Each employee has only one job.

• Each job can be held by many employees.

• Many employees may receive many benefits.

• Many benefits may be selected by many employees(though, while they may not select any benefits—any dependents of employees may be on an employee’s plan).

Notes:

• Employee/Dependenttables must use suitable attributes (See Assignment Guidelines)

In Addition:

• Employee:SSN, DOB, start/end dates, salary;

• Dependent:same information as their associated employee(though, not start/end dates),date added(as dependent),type of relationship: e.g., father, mother, etc.

• Job: title(e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)

• Benefit:name(e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)

• Plan:type(single, spouse, family), cost, election date(plans must be unique)

• Employeehistory:jobs, salaries, and benefit changes, as well as whomade the changeand why;

• Zero Filled data: SSN, zip codes (not phone numbers: US area codesnot below 201, NJ);

• *All* tables must include notesattribute.

#### README.md file should include the following items:

• Screenshot of A1 ERD

• Ex.1-7 SQL Solution

• git commands w/short descriptions

#### Git commands w/short descriptions:

1. git init - Creates a new local repository with the specified name
2. git status - Lists all new or modified files to be commited
3. git add - Snapshots the file in preparation for versioning
4. git commit - Records file snapshots permanently in version history  
5. git push - Uploads all local branch commits to GitHub
6. git pull - Downloads bookmark history and incorporates changes
7. git branch - Lists all local branches in the current repository

#### Assignment Screenshots:

*Screenshot of AMPPS running*:

![Screenshot of AMMPS running](img/ampps_r.png)

*Screenshot of A1 ERD test*:

![Screenshot of A1 ERD](img/a1_ERD.png)

*Screenshot of A1 Ex1*:

![Screenshot of A1 Ex1](img/a1_ex1.png)

*Screenshot of A1 Ex2*:

![Screenshot of A1 Ex2](img/a1_ex2.png)

*Screenshot of A1 Ex3*:

![Screenshot of A1 Ex3](img/a1_ex3.png)

*Screenshot of A1 Ex3*:

![Screenshot of A1 Ex3](img/a1_ex3_1.png)

*Screenshot of A1 Ex4*:

![Screenshot of A1 Ex4](img/a1_ex4.png)

*Screenshot of A1 Ex5*:

![Screenshot of A1 Ex5](img/a1_ex5.png)

*Screenshot of A1 Ex6*:

![Screenshot of A1 Ex6](img/a1_ex6.png)

*Screenshot of A1 Ex7*:

![Screenshot of A1 Ex7](img/a1_ex7.png)

*Screenshot of A1 Ex7*:

![Screenshot of A1 Ex7](img/a1_ex7_1.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")