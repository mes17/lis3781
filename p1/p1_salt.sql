-- Salt --
DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN
DECLARE x, y INT;
SET x=1;

select count(*) into y from person;

WHILE x <= y DO

    SET @salt=RANDOM_BYTES(64);
    SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
    SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512));

    update person
    set per_ssn=@ssn, per_salt=@salt
    where per_id=x;

SET x = x + 1;

END WHILE;

END$$
DELIMITER ;
call CreatePersonSSN();
      

