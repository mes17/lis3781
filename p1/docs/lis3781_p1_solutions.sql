--1. Create a view that displays attorneys’ *full* names, *full* addresses, ages, hourly rates, the bar names 
--that they’ve passed, as well as their specialties, sort by attorneys’ last names.

select '1) Create a view that displays attorneys\' *full* names addresses, ages, hourly rates, \nthe bar 
names t-that they\'ve passed, as well as thier specialties, sort by attorneys\' last names.' as '; -- somthing wrong here

drop VIEW if exists v_attorney_info;
CREATE VIEW v_attorney_info AS
    select
    concat(per_lname, ",", per_fname) as name,
    concat(per_street, ",", per_city,",", per_state, " " per_zip) as address,
    TIMESTAMPDIFF(year, per_dob, now()) as age,
    CONCAT('$', FORMAT(aty_hourly_rate, 2)) as hourly_rate,
    bar_name, spc_type
    from person
        natural join attorney
        natural join bar
        natural join specialty
        order by per_lname;

--2. Create a stored procedure that display show many judges were born in each month of the year, sorted by month.

drop procedure if exists sp_num_judges_born_by_month;
DELIMITER //
CREATE PROCEDURE sp_num_judges_born_by_month()
BEGIN
    select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
    from person
    natural join judge
    group by month, month_name 
    order by month;
END //
DELIMITER ;

--3. Create a stored procedure that displays*all* case types and descriptions, as well as judges’ *full*names, 
--*full* addresses, phone numbers, years in practice, for cases that they presided over, with their start and end dates, sort by judges’ last names.

drop procedure if exists sp_cases_and_judges;
DELIMITER //
CREATE PROCEDURE sp_cases_and_judges()
BEGIN

select per_id, cse_id, cse_type, cse_description,
    concat(per_fname," ", per_lname) as name,
    concat('(',substring(phn_num, 1,3), ')', substring(phn_num, 4,3), '-', substring(phn_num, 7,4)) as judge_phone_num,
    phn_type,
    jud_years_in_practice,
    cse_start_date,
    cse_end_date
from person
    natural join judge
    natural join `case`
    natural join phone
where per_type='j'
order by per_lname;

END //
DELIMITER ;

select 'calling sp_cases_and_judges()' as '';

CALL sp_cases_and_judges();
do sleep(5);
drop procedure if exists sp_cases_and_judges;


--4. Create a trigger that automatically adds a record to the judge history table for every record addedto the judge table.
select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.' as'';
do sleep(5);

-- Note: technically, a judge could already be in the person table, yet not added to the judge table. Or, here, add a new person.

-- add 16th person (a judge), before adding person to judge table (salt and hash per_ssn, and store unique salt, per_salt)

select 'show person data *before* adding person record' as '';

select per_id, per_fname, per_lname from person;

do sleep(5);

-- give person a unique randomized salt, then hash and salt SSN

SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user
SET @num=000000000; -- Note: already provided random SSN from 111111111 - 999999999, inclusive above
SET @ssn=unhex(sha2(concat(@salt, @num), 512)); -- salt and hash person's SSN 000000000

INSERT INTO person

(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
values

(NULL, @ssn, @salt, ‘Bobby’, ‘Sue’, '123 Main St', ‘Panama City Beach’, 'FL', 324530221, 'bsue@fl.gov', '1962-05-16', 'j', ‘new district judge’);
select ‘show person data *after* adding person record’ as ;

select per_id, per_fname, per_lname from person;

do sleep(5);

select 'show judge/judge_hist data *before* AFTER INSERT trigger fires (trg_judge_history_after_insert)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

drop TRIGGER if exists trg_judge_history_after_insert;
DELIMITER //
CREATE PROCEDURE trg_judge_history_after_insert()
AFTER INSERT ON judge
FOR EACH ROW
BEGIN

INSERT INTO judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
(
    NEW.per_id, NEW.crt_id,current_timestamp(), 'i', NEW.jhs_salary,
    concat("modifying user: ", user(),"NOTES: ", NEW.jud_notes)
);
END //
DELIMITER ;

select 'fire tigger by instering record into judge table' as '';
do sleep(5);
INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
((select count(per_id) from person), 3, 175000, 31, 'transferred from neighboring jurisdiction');

select 'show judge/judge_hist data *before* AFTER INSERT trigger fires (trg_judge_history_after_insert)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);


--5. Create a trigger that automatically adds a record to the judge history table for every record modified in the judge table.

drop TRIGGER if exists trg_judge_history_after_update;
DELIMITER //
CREATE PROCEDURE trg_judge_history_after_update()
AFTER INSERT ON judge
FOR EACH ROW
BEGIN

INSERT INTO judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
    values
    (
    NEW.per_id, NEW.crt_id,current_timestamp(), 'i', NEW.jhs_salary,
    concat("modifying user: ", user(),"NOTES: ", NEW.jud_notes)
    );
END //
DELIMITER ;

select 'fire tigger by updating latest judge entry (salary and notes)' as '';
do sleep(5);

UPDATE judge
SET jud_salary=190000, jud_notes='senior justice -longest serving member'
WHERE per_id=16;

select 'show judge/judge_hist data *after* AFTER INSERT trigger fires (trg_judge_history_after_update)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);


--6. Create a one-time event that executes one hour following itscreation, the event should add a judge record (one more than the required five records), 
--have the event call a stored procedure that adds the record (name it one_time_add_judge).

select '1) check event_scheduler' as '';
SHOW VARIABLES LIKE 'event_scheduler';
do sleep(5);

select '2) if not, turn it on...' as '';
SET GLOBAL event_scheduler = ON;

select '3) recheck event_schedular' as '';
SHOW VARIABLES LIKE 'event_schedular' as '';
do sleep(5);

select 'show judge/judge_hist data *before* event fires (one_time_add_judge)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

drop procedure if exists sp_add_judge_record;
DELIMITER //

CREATE PROCEDURE sp_add_judge_record()
BEGIN
    INSERT INTO judge
    (per_id, crt_id, jud_salary, jud_years_in_practicem jud_notes)
    values
    (6, 1, 110000, 0, concat("New judge was former attorney. ", "Modifying event creator:" , current_user()));
END //
DELIMITER ;


drop EVENT if exists one_time_add_judge;
DELIMITER //
CREATE EVENT IF NOT exists one_time_add_judge
ON SCHEDULE
    AT NOW() + INTERVAL 1 HOUR
COMMENT 'adds a judge record only one-time'
DO
BEGIN
    CALL sp_add_judge_record();
END //
DELIMITER ;

select 'show events from mes17_p1;' as '';
SHOW EVENTS FROM mes17_p1;
do sleep(5);

select 'show state of event sceduler: show processlist;' as '';
show processlist;
do sleep(5);

select 'show judge/judge_hist data *after* event fires (one_time_add_judge)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

--Extra Credit. Create a scheduled event that will run every two months,beginning in three weeks, and runs for the next four years, starting from the creation date. 
--The event should not allow more than the first 100 judge histories to be stored, thereby removing all others(name it remove_judge_history).

drop EVENT if exists remove_judge_history;
DELIMITER //
CREATE EVENT IF NOT exists remove_judge_history
ON SCHEDULE
    EVERY 2 MONTH
STARTS NOW() + INTERVAL 3 WEEK
ENDS NOW () + INTERVAL 4 YEAR
COMMENT "keeps only the first 100 judge records"
DO
BEGIN
    DELETE FROM judge_hist where jhs_id > 100;
END //
DELIMITER ;







--- IDK 
select 'Step a) Display all persons DOB months: testing MYSQL monthname() function' as '';

select per_id, per_fname, per_lname, per_dob, monthname(per_dob) from person;
do sleep(5);

select 'Step b) Display petinent judge data' as '';

select p.per_id, per_fname, per_lname, per_dob, per_type
from person as p
    natural join judges as j;
do sleep(5);