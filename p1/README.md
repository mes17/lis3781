> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Managements

## Matthew Stoklosa

### Project 1 Requirements:

*Four parts:*

1. Business Rules
2. Questions
3. Entity Relationship Diagram, and SQL Statements
4. Bitbucket repo links: this assignment  

#### P1 Database Business Rules:

As the lead DBA for a local municipality, you are contacted by the city council to design a database in order to track and document the city’scourt case data.

Some report examples: Which attorney is assigned to what case(s)?

How many unique clients have cases(be sure to add a client tomore than one case)?

How many cases has each attorney been assigned, and names of their clients (return number and names)?

How many cases does each client have with the firm(return a name and number value)?

Which types ofcases does/did each client have/had and their start and end dates?

Which attorney is associated to which client(s), and to which case(s)?

Names of three judges with the most number of years in practice, include number of years.

Also, include the following business rules:

- An attorney is retained by (or assigned to)one or more clients,for each case.
- A client has(or is assigned to) one or more attorneys for each case.
- An attorney has one or more cases.
- A client has one or more cases.
- Each court has one or more judges adjudicating.
- Each judge adjudicates upon exactly one court.
- Each judge may preside over more than one case.
- Each case that goes to court is presided over by exactly one judge.
- A person can have more than one phone number.Notes:
- Attorney data must include social security number, name, address, office phone, home phone,e-mail,start/end dates, dob, hourly rate, years in practice,bar(may be more than one-multivalued), specialty(may be more than one-multivalued).
- Client data must include social security number, name, address, phone, e-mail, dob.
- Case datamust include type, description, start/end dates.
- Court data must include name, address, phone, e-mail, url.
- Judge data must include same information as attorneys(except bar, specialtyand hourly rate;instead, use salary).
- Must track judge historical data—tenure at each court (i.e., start/end dates), and salaries.
- Also, history will track which courts judges presided over,if they took a leave of absence, or retired.
- All tables must have notes.

NB: In some designs, the common attributes would be inherited from a “person” table.

Additional Notes:

- Social security numbers, should be unique, and must use SHA2 hashing with salt.
- Entities must be included in logical layers (colored appropriately).
- ERD MUST include relationships and cardinalities.

#### README.md file should include the following items:

• Screenshot of P1 ERD

• Screenshot of P1 SQL Questions

#### Assignment Screenshots and Links:

*Screenshot of P1 ERD*:

![Screenshot of P1 ERD](img/p1_ERD.png)

*Screenshot of P1 SQL Questions 3*:

![Screenshot of P1 SQL Question 3](img/p1_sql.png)

*P1 docs: p1.mwb and p1.sql*:

[P1 MWB File](docs/lis3781_p1.mwb "P1 ERD in .mwb format")

[P1 SQL File](docs/lis3781_p1_solutions.sql "P1 SQL Script")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[P1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocations/ "Bitbucket Station Locations")