> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Managements

## Matthew Stoklosa

### Assignment 5 Requirements:

*Four parts:*

1. Business Rules
2. Questions
3. Entity Relationship Diagram
4. SQL Code

#### A5 Database Business Rules:

Expanding upon the high-volumehome office supply company’s data tracking requirements, the CFO requests your services again to extend the data model’s functionality. The CFO has read about the capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a smaller data mart as a test platform. He is under pressure from the members of the company’s board of directors who want to reviewmore detailed salesreports based upon the following measurements:
1. Product
2. Customer
3. Sales representative
4. Time(year, quarter, month, week, day, time)
5. Location

Furthermore, the board members want location to be expanded to include the following characteristics of location:
1. Region
2. State
3. City
4. Store

#### README.md file should include the following items:

• Screenshot of A5 ERD

• Screenshot of A5 tables

• A5 solutions

#### Assignment Screenshots and links:

*Screenshot of A5 ERD*:

![Screenshot of A5 ERD](img/a5_ERD.png)

*Screenshot of A5 tables*:

![Screenshot of A5 tables](img/a5_tables_1.png)

*Screenshot of A5 tables*:

![Screenshot of A5 tables](img/a5_tables_2.png)

*Screenshot of A5 tables*:

![Screenshot of A5 tables](img/a5_tables_3.png)

*A5 docs: A5_code.sql*:

[A5 SQL code](docs/a5_code.sql "A5 SQL code")
